import { controls } from '../../constants/controls';
import { createElement } from '../helpers/domHelper';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {

    const healthBarsContainer = document.getElementsByClassName('arena___health-bar');
    const healthBars = [...healthBarsContainer];
    const statusViewContainer = document.getElementsByClassName('arena___health-indicator');
    const statusViews = [...statusViewContainer];
    const statusInfo = {
      block: false,
      currentHealth: 100,
      timeOfCombo: Date.now(),
      comboInput: []
    };
    const playerOne = {
      ...firstFighter,
      ...statusInfo,
      healthBar: healthBars[0],
      statusView: statusViews[0],
      position: 'left'
    };
    const playerTwo = {
      ...secondFighter,
      ...statusInfo,
      healthBar: healthBars[1],
      statusView: statusViews[1],
      position: 'right'
    };

    function showStatus(fighter, text) {
      if (document.getElementById(`${fighter.position}-status`)) {
        document.getElementById(`${fighter.position}-status`).remove();
      }
      const statusMarker = createElement({
        tagName: 'div',
        className: 'arena___status',
        attributes: { id: `${fighter.position}-status` }
      });
      statusMarker.innerText = text;
      statusMarker.style.opacity = '1';
      fighter.statusView.appendChild(statusMarker);
      setInterval(() => {
        if (parseFloat(statusMarker.style.opacity) > 0) {
          statusMarker.style.opacity = (statusMarker.style.opacity - 0.008).toString();
        } else {
          statusMarker.remove();
        }
      }, 10);
    }

    function attack(attacker, defender) {
      let totalDamage;
      if (attacker.comboInput.length === 3) {
        showStatus(attacker, 'Co-co-co combo!');
        totalDamage = 2 * attacker.attack;
      } else {
        totalDamage = getDamage(attacker, defender);
        if (attacker.block) {
          showStatus(attacker, 'You can\'t have your cake and eat it too');
          return void 0;
        }else if (defender.block) {
          showStatus(defender, 'Blocked!');
          return void 0;
        }else if (!totalDamage) {
          showStatus(attacker, 'Missed!');
          return void 0;
        }
      }
      showStatus(defender, `-${totalDamage.toFixed(1)}`);
      defender.currentHealth = defender.currentHealth - totalDamage / defender.health * 100;
      defender.healthBar.style.width = `${Math.max(0,defender.currentHealth)}%`;
      if (defender.currentHealth <= 0) {
        document.removeEventListener('keydown', keyPressed);
        document.removeEventListener('keyup', keyReleased);
        resolve(attacker);
      }
    }

    function comboHit(fighter, code) {
      const currentTime = Date.now();
      if (currentTime - fighter.timeOfCrit < 10000) {
        return false;
      }
      if (!fighter.comboInput.includes(code)) {
        fighter.comboInput.push(code);
      }
      if (fighter.comboInput.length === 3) {
        fighter.timeOfCrit = currentTime;
        return true;
      }
      return false;
    }

    function keyPressed(event) {
      if (!event.repeat) {
        switch (event.code) {
          case controls.PlayerOneAttack: {
            attack(playerOne, playerTwo);
            break;
          }
          case controls.PlayerTwoAttack: {
            attack(playerTwo, playerOne);
            break;
          }
          case controls.PlayerOneBlock: {
            playerOne.block = true;
            break;
          }
          case controls.PlayerTwoBlock: {
            playerTwo.block = true;
            break;
          }
        }
        if (controls.PlayerOneCriticalHitCombination.includes(event.code) && comboHit(playerOne, event.code)) {
          attack(playerOne, playerTwo);
        }else if (controls.PlayerTwoCriticalHitCombination.includes(event.code) && comboHit(playerTwo, event.code)) {
          attack(playerTwo, playerOne);
        }
      }
    }

    function keyReleased(event) {
      switch (event.code) {
        case controls.PlayerOneBlock:
          playerOne.block = false;
          break;
        case controls.PlayerTwoBlock:
          playerTwo.block = false;
          break;
      }
      //if release key - fold a combination
      if (playerOne.comboInput.includes(event.code)) {
        playerOne.comboInput.splice(playerOne.comboInput.indexOf(event.code), 1);
      }else if (playerTwo.comboInput.includes(event.code)) {
        playerTwo.comboInput.splice(playerTwo.comboInput.indexOf(event.code), 1);
      }
    }
    document.addEventListener('keydown', keyPressed);
    document.addEventListener('keyup', keyReleased);
  });
}

export function getDamage(attacker, defender) {
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getHitPower(fighter) {
  const criticalHitChance = fighter.comboInput === 3 ? 2 : Math.random() + 1;
  return fighter.attack * criticalHitChance;
}

export function getBlockPower(fighter) {
  const dodgeChance = Math.random() + 1;
  return fighter.defense * dodgeChance;
}