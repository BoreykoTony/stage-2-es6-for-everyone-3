import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`
  });

  if (typeof fighter !== 'undefined') {
    const fighterImg = createFighterImage(fighter);
    const fighterDetails = createDetails(fighter);
    if (position === 'left') {
      fighterElement.append(fighterDetails, fighterImg);
    }else{
      fighterElement.append(fighterImg, fighterDetails);
    }
  }
  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = {
    src: source,
    title: name,
    alt: name
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes
  });
  return imgElement;
}

export function createDetails(fighter) {
  const fighterName = createName(fighter);
  const fighterDetails = createCharacteristic(fighter);
  const fighterDetailsWrapper = createElement({
    tagName: 'div',
    className: 'fighter-details-wrapper'
  });
  fighterDetailsWrapper.append(fighterName, fighterDetails);
  return fighterDetailsWrapper;
}

function createName(fighter) {
  const fighterName = createElement({ tagName: 'h4' });
  fighterName.innerText = fighter.name;
  return fighterName;
}

function createCharacteristic(fighter) {
  const fighterDetails = createElement({});
  fighterDetails.innerHTML = `
      <ul>
        <li>
          <p>Attack - ${fighter.attack}</p>
        </li>
        <li>
          <p>Deffence - ${fighter.defense}</p>
        </li>
        <li>
          <p>Health - ${fighter.health}</p>
        </li>
      </ul>
    `;
  return fighterDetails;
}
