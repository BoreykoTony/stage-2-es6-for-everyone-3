import { showModal } from "./modal";

export function showWinnerModal(fighter) {
  const winnerInfo = {
    title: fighter.name + ' wins',
    bodyElement: fighter.currentHealth == 100 ? "Flawless victory" : fighter.currentHealth > 50 ? "Good victory" : "Satisfactory victory"
  }
  showModal(winnerInfo);
}